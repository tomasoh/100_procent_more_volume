#pragma once

#include <GLFW/glfw3.h>
#include <AntTweakBar.h>

#include "rendering/RenderEngine.hpp"
#include "rendering/Camera.hpp"
#include "scene/Scene.hpp"

const float CAMERA_MOVE_FACTOR = 1.5;
const float CAMERA_ROTATE_FACTOR = 40.0;

/**
 * Application manages the entire application. Its main purpose is to 
 * set up and handle the main window with its tweak bars.
 */
class Application {

    GLFWwindow* window;
    
    TwBar* tweakBar;
    
    RenderEngine* engine;
    Camera camera;
    Scene* scene;
    
    Application() {};

public:
    ~Application();
    /**
     * Get a reference to the singleton instance of Application.
     */
    static Application& getInstance();
    
    Application(const Application&) = delete;
    void operator=(const Application&) = delete;
    
    void init(SceneType sceneType);
    void runMainLoop();
    void terminate();
    
    void updateCameraFromKeyboard(float deltaT);
    
    void resizeWindow(int width, int height);
    void setCloseFlag();
    
    
    static void onGLFWMouseButtonEvent(GLFWwindow*, int button, int action, int /*mods*/);
    static void onGLFWMousePositionEvent(GLFWwindow*, double x, double y);
    static void onGLFWKeyEvent(GLFWwindow*, int key, int /*scancode*/, int action, int /*mods*/);
    static void onGLFWWindowResizeEvent(GLFWwindow* window, int width, int height);
};

    
