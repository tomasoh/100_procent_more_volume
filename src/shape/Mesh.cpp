#include <iostream>
#include <vector>

#include <glad/glad.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include "shape/Mesh.hpp"
#include "rendering/Shader.hpp"


void MeshInstance::uploadMaterial(Shader& shader) {
    shader.setUniform1f("material.diffuseFactor", material.diffuseFactor);
    shader.setUniform1f("material.specularFactor", material.specularFactor);
    shader.setUniform1f("material.specularSharpness", material.specularSharpness);
    shader.setUniform1f("material.ambientFactor", material.ambientFactor);
}

void MeshInstance::drawInstance(const glm::mat4& worldToProj, Shader& shader) {
    shader.setUniformMatrix4fv("modelToProj", worldToProj*modelToWorld);
    shader.setUniformMatrix4fv("modelToWorld", modelToWorld);
    
    uploadMaterial(shader);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseTexture);
    mesh->drawMesh(shader.getShaderId());
}


void MeshInstance::drawInstanceAsShadow(Shader& shader) {
    shader.setUniformMatrix4fv("modelToWorld", modelToWorld);
    mesh->drawMeshAsShadow();
}


void MeshInstance::setDiffuseTexture(uint textureId) {
    diffuseTexture = textureId;
}

void MeshInstance::setMaterial(const Material& mat) {
    material = mat;
}

void MeshInstance::applyTransformation(const glm::mat4& transMat) {
    modelToWorld = transMat*modelToWorld;
}



void Mesh::uploadModelToGPU(const std::vector<glm::vec3>& vertexArray, 
                            const std::vector<glm::vec2>& uvArray, 
                            const std::vector<glm::vec3>& normalArray, 
                            const std::vector<uint>& indexArray) {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    // Buffer for vertices
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertexArray.size()*sizeof(glm::vec3), &vertexArray[0], GL_STATIC_DRAW);
    
    // Buffer for normals
    if(not normalArray.empty()) {
        glGenBuffers(1, &normalBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
        glBufferData(GL_ARRAY_BUFFER, normalArray.size()*sizeof(glm::vec3), &normalArray[0], GL_STATIC_DRAW);
    }
    
    // Buffer for uvs
    if(not uvArray.empty()) {
        glGenBuffers(1, &uvBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
        glBufferData(GL_ARRAY_BUFFER, uvArray.size()*sizeof(glm::vec2), &uvArray[0], GL_STATIC_DRAW);
    }
    
    // Buffer for vertex indecies
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexArray.size()*sizeof(uint), &indexArray[0], GL_STATIC_DRAW);
}


Mesh::Mesh(const std::vector<glm::vec3>& vertexArray, 
                const std::vector<glm::vec2>& uvArray, 
                const std::vector<glm::vec3>& normalArray, 
                const std::vector<uint>& indexArray) {
    indeciesSize = indexArray.size();
    uploadModelToGPU(vertexArray, uvArray, normalArray, indexArray);
}

void Mesh::drawMesh(uint shaderId) {
    glBindVertexArray(vao);
    
    int loc = glGetAttribLocation(shaderId, "inPosition");
    if(loc >= 0) {
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0); 
        glEnableVertexAttribArray(loc);
    }
    else
        std::cerr << "DrawMesh, cannot find inPosition" << 
                    " in shader program " << shaderId << "." << std::endl;

    if(normalBuffer) {
        loc = glGetAttribLocation(shaderId, "inNormal");
        if (loc >= 0) {
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
            glEnableVertexAttribArray(loc);
        }
        else
            std::cerr << "DrawMesh, cannot find inNormal"<< 
                    " in shader program " << shaderId << "." << std::endl;
    }
    
    if(uvBuffer) {
        loc = glGetAttribLocation(shaderId, "inTexCoord");
        if (loc >= 0) {
            glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
            glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
            glEnableVertexAttribArray(loc);
        }
        else
            std::cerr << "DrawMesh, cannot find inUV" << 
                    " in shader program " << shaderId << "." << std::endl;
    }
    
    glDrawElements(GL_TRIANGLES, indeciesSize, GL_UNSIGNED_INT, 0L);
}

void Mesh::drawMeshAsShadow() {
    glBindVertexArray(vao);
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(VERT_ATTRIB_LOC, 3, GL_FLOAT, GL_FALSE, 0, 0); 
    glEnableVertexAttribArray(VERT_ATTRIB_LOC);
    
    glDrawElements(GL_TRIANGLES, indeciesSize, GL_UNSIGNED_INT, 0L);
}
