#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "shape/Mesh.hpp"

const std::vector<glm::vec3> QUAD_VERTICES = {{-1.0, 1.0, 0.0}, {-1.0, -1.0, 0.0}, {1.0, 1.0, 0.0}, {1.0, -1.0, 0.0}};
const std::vector<glm::vec2> QUAD_UVS = {{0.0, 1.0}, {0.0, 0.0}, {1.0, 1.0}, {1.0, 0.0}};
const std::vector<uint> QUAD_INDICES = {0, 1, 2, 1, 3, 2};


class Quad : public Mesh {
public:
    Quad() : Mesh(QUAD_VERTICES, QUAD_UVS, {}, QUAD_INDICES) {};
};
