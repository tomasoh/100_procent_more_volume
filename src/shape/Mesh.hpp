#pragma once

#include <vector>

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "rendering/Shader.hpp"
#include "rendering/Material.hpp"

class Mesh;

// Attibute locations in the shaders:
const uint VERT_ATTRIB_LOC = 0;


/**
 * An instance of a Mesh in the scene. MeshInstances shares a single Mesh but might have different material and textures.
 * They can also apply different model to world transformations to the instance of the mesh.
 */
class MeshInstance {
    Mesh* mesh;
    glm::mat4 modelToWorld;
    
    Material material;
    uint diffuseTexture;
    
    void uploadMaterial(Shader& shader);
    
public:
    MeshInstance(Mesh* _mesh, glm::mat4 _modelToWorld) : 
                mesh{_mesh}, modelToWorld{_modelToWorld} {};
    ~MeshInstance() = default;
    
    /**
     * Draws this instance of a mesh as seen by the worldToProj matrix. Uses
     * the currently active GL shader program.
     */
    void drawInstance(const glm::mat4& worldToProj, Shader& shader);
    
    /**
     * Draw this instance to a shadow map. Uses the currently active GL 
     * shader program.
     */
    void drawInstanceAsShadow(Shader& shader);
    
    void setDiffuseTexture(uint textureId);
    void setMaterial(const Material& mat);
    
    /**
     * Applies a transformation matrix to the current modelToWorld matrix.
     */
    void applyTransformation(const glm::mat4& transMat);
};


/**
 * A class referencing vertex data for a mesh on the GPU.
 */
class Mesh {
    uint vao;
    uint vertexBuffer, normalBuffer = 0, uvBuffer = 0;
    uint indexBuffer;
    uint indeciesSize = 0; // Number of indices in the loaded mesh
    
    void loadModelFromOBJ(const std::string& filename);
    void uploadModelToGPU(const std::vector<glm::vec3>& vertexArray, 
                            const std::vector<glm::vec2>& uvArray, 
                            const std::vector<glm::vec3>& normalArray, 
                            const std::vector<uint>& indexArray);
public:
    Mesh(const std::vector<glm::vec3>& vertexArray, 
                    const std::vector<glm::vec2>& uvArray, 
                    const std::vector<glm::vec3>& normalArray, 
                    const std::vector<uint>& indexArray);
    ~Mesh() = default;
    
    void drawMesh(uint shaderId);
    void drawMeshAsShadow();
};
