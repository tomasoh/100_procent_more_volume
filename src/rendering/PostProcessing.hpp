#pragma once

#include "Shader.hpp"

/**
 * Post processing parameters to the shaders. Mostly for tone mapping.
 */
struct PostProcessingData {
    bool onlyVolume = false;
    float gamma = 2.7f;
    float exposure = 5.0f;
    
    void uploadToShader(Shader& shader) const;
};
