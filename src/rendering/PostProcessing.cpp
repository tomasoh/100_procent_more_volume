#include "PostProcessing.hpp"


void PostProcessingData::uploadToShader(Shader& shader) const {
    shader.setUniform1i("onlyVolume", onlyVolume);
    shader.setUniform1f("gamma", gamma);
    shader.setUniform1f("exposure", exposure);
}
