#pragma once

#include <GLFW/glfw3.h>

#include "rendering/Shader.hpp"
#include "rendering/FrameBuffer.hpp"
#include "rendering/Camera.hpp"
#include "rendering/PostProcessing.hpp"
#include "shape/Mesh.hpp"
#include "scene/Scene.hpp"

/**
 * The RenderEngine manages the rendering pipeline. It is a singleton
 * and the instance can be accesed via the getInstance function. 
 * The engine is invoked on every frame to render a specific scene. 
 */
class RenderEngine {
    
    int width, height;
    
    Shader objectShader;
    Shader volumetricShader;
    Shader toneMappingShader;
    
    PostProcessingData postProcessingData;
    
    FrameBuffer volumetricFramebuffer;
    FrameBuffer hdrFramebuffer;
    Mesh* quadMesh;


    RenderEngine(int _width=640, int _height=400);
    ~RenderEngine();
    
    void drawQuad();

    uint query[4];
public:
    // Rendertimes, Shadow map, scene draw, volumetric pass, post process and total time
    double msTime[5];
    
    RenderEngine(const RenderEngine&) = delete;
    void operator=(const RenderEngine&) = delete;
    
    /**
     * Returns the singleton RenderEngine.
     */
    static RenderEngine& getInstance();

    PostProcessingData& getPostProcessingData() {return postProcessingData;}
    
    /**
     * Resize the framebuffers in the RenderEngine. Must be called when the screen size changes.
     */
    void resize(int width, int height);
    
    /**
     * Render a Scene as seen from a Camera using this RenderEngine. Result will be written to 
     * default screen frame buffer.
     */
    void render(Scene& scene, const Camera& camera);
    
};
