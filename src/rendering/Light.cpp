
#include <glm/gtx/transform.hpp>

#include "glad/glad.h"
#include "rendering/Light.hpp"


static Shader pointLightShader;
static Shader spotLightShader;


Light::Light(const glm::vec3& _position, const glm::vec3& _color, float _intensity) : type{POINT_LIGHT}, 
            position{_position}, color{_color}, intensity{_intensity} {
    initShadowMap();
}


Light::Light(const glm::vec3& _position, const glm::vec3& _direction, float _cutOffAngle, 
             const glm::vec3& _color, float _intensity) :
                type{SPOT_LIGHT}, position{_position}, direction{_direction}, 
                cutOffAngle{_cutOffAngle}, color{_color}, intensity{_intensity} {
    initShadowMap();
}


Light::~Light() {
    glDeleteTextures(0, &shadowMapId);
    glDeleteFramebuffers(0, &shadowFBOId);
}


void Light::initShaders() {
    pointLightShader.initialize("shaders/shadow_mapping_point.vert", 
                                "shaders/shadow_mapping_point.geom",
                                "shaders/shadow_mapping_point.frag");
    spotLightShader.initialize("shaders/shadow_mapping_spot.vert", 
                                "shaders/shadow_mapping_spot.frag");
}


void Light::initShadowMap() {
    glGenTextures(1, &shadowMapId);
    
    // Init shadow map textures
    if(type == POINT_LIGHT) {
        glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMapId);
        shadowMapSize = POINT_LIGHT_SHADOW_MAP_SIZE;
        for(uint i = 0; i < 6; ++i) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT,
                    shadowMapSize, shadowMapSize, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }
    }
    else if(type == SPOT_LIGHT) {
        glBindTexture(GL_TEXTURE_2D, shadowMapId);
        shadowMapSize = SPOT_LIGHT_SHADOW_MAP_SIZE;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapSize, shadowMapSize, 
                0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    
    // Init FBO
    glGenFramebuffers(1, &shadowFBOId);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowFBOId);
    if(type == POINT_LIGHT) {
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowMapId, 0);
    }
    else if(type == SPOT_LIGHT) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowMapId, 0);
    }
    glReadBuffer(GL_NONE);
    glDrawBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void Light::uploadToShader(Shader& shader, const std::string& variable,
                            int texUnit) {
    if(type == POINT_LIGHT) {
        glActiveTexture(GL_TEXTURE0 + texUnit);
        glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMapId);
    }
    else if(type == SPOT_LIGHT) {
        glActiveTexture(GL_TEXTURE0 + texUnit + MAX_LIGHTS);
        glBindTexture(GL_TEXTURE_2D, shadowMapId);
    }
    shader.setUniform1i(variable + ".type", type);
    shader.setUniform3fv(variable + ".position", position);
    shader.setUniform3fv(variable + ".direction", direction);
    shader.setUniform1f(variable + ".spotCutOff", glm::cos(glm::radians(cutOffAngle)));
    shader.setUniform3fv(variable + ".color", color);
    shader.setUniform1f(variable + ".intensity", intensity);
    shader.setUniform1f(variable + ".shadowMapZFar", SHADOW_Z_FAR);
    shader.setUniformMatrix4fv(variable + ".spotWorldToProj", worldToProj);
}


void Light::renderShadowMap(Scene& scene) {
    if(!scene.isDirty() and !dirtyShadowMap) {
        // Shadow map has not changed, no need to render it again
        return;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, shadowFBOId);
    glViewport(0, 0, shadowMapSize, shadowMapSize);
    glClear(GL_DEPTH_BUFFER_BIT);
    
    if(type == POINT_LIGHT) {
        const glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), 1.0f, SHADOW_Z_NEAR, SHADOW_Z_FAR);
        std::vector<glm::mat4> shadowTransforms;
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0))); /* Right */
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(-1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0))); /* Left */
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,1.0,0.0), glm::vec3(0.0, 0.0, 1.0))); /* Back */
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,-1.0,0.0), glm::vec3(0.0,0.0,-1.0))); /* Front */
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,0.0,1.0), glm::vec3(0.0,-1.0,0.0))); /* Up */
        shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,0.0,-1.0), glm::vec3(0.0,-1.0,0.0))); /* Down */
        
        pointLightShader.use();
        for(uint i = 0; i < 6; ++i) {
            const std::string s = "cubeMapMatrices[" + std::to_string(i) + "]";
            pointLightShader.setUniformMatrix4fv(s, shadowTransforms[i]);
        }
        
        pointLightShader.setUniform1f("zFar", SHADOW_Z_FAR);
        pointLightShader.setUniform3fv("lightPosition", position);
        scene.drawSceneToShadowMap(pointLightShader);
    }
    else if(type == SPOT_LIGHT) {
        const glm::mat4 shadowProj = glm::perspective(glm::radians(cutOffAngle*2), 1.0f, SHADOW_Z_NEAR, SHADOW_Z_FAR);
        const glm::mat4 shadowView = glm::lookAt(position, position+direction, Z_AXIS);
        worldToProj = shadowProj*shadowView;
        
        spotLightShader.use();
        spotLightShader.setUniformMatrix4fv("worldToProj", worldToProj);
        scene.drawSceneToShadowMap(spotLightShader);
    }
    dirtyShadowMap = false;
}


void Light::updateLightPosition(const glm::vec3& newPosition) {
    position = newPosition;
    dirtyShadowMap = true;
}


void Light::updateLightDirection(const glm::vec3& newDirection) {
    direction = newDirection;
    dirtyShadowMap = true;
}


void Light::updateSpotConeAngle(float newCutOffAngle) {
    cutOffAngle = newCutOffAngle;
    dirtyShadowMap = true;
}


void Light::updateLightColor(const glm::vec3& newColor) {
    color = newColor;
}
