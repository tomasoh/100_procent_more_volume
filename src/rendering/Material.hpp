#pragma once

#include <glm/vec3.hpp>

/**
 * A struct containing definition of a surface material.
 */
struct Material {
    glm::vec3 diffuseColor;
    float diffuseFactor;
    float specularFactor;
    float specularSharpness;
    float ambientFactor;
};
