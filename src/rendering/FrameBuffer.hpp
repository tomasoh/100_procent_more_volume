#pragma once
#include <sys/types.h>

#include "utils/Types.hpp"

void useScreenFrameBuffer();

/**
 * FrameBuffer class manages a frame buffer containing a color buffer and a Z-buffer.
 */
class FrameBuffer {
    uint fboId;
    uint colorBuffer;
    uint zBuffer;
    
public:
    FrameBuffer(uint width, uint height);
    ~FrameBuffer();
    
    void resize(uint width, uint height);
    
    void use();
    uint getColorBufferId() {return colorBuffer;}
    uint getZBufferId() {return zBuffer;}
};

