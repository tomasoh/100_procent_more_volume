
#include <glm/gtc/matrix_transform.hpp>

#include "rendering/Camera.hpp"


void Camera::updateOrientation(const glm::vec3& _cameraPosition, 
                const glm::vec3& _direction, const glm::vec3& _up) {

    cameraPosition = _cameraPosition;
    direction = glm::normalize(_direction);
    up = _up;
    
    float len = glm::sqrt(direction.x*direction.x + direction.y*direction.y);
    pitch = glm::degrees(glm::atan(direction.z, len));
    yaw = glm::degrees(glm::atan(direction.y, direction.x));
    
    glm::vec3 lookAt = cameraPosition + direction;
    worldToView = glm::lookAt(cameraPosition, lookAt, up);
    worldToProj = viewToProj*worldToView;
}


void Camera::updateProjection(float _fov, float _zNear, float _zFar) {
    fov = _fov;
    zNear = _zNear;
    zFar = _zFar;
    viewToProj = glm::perspective(fov, aspectRatio, zNear, zFar);
    worldToProj = viewToProj*worldToView;
}


void Camera::updateAspectRatio(float _aspectRatio) {
    aspectRatio = _aspectRatio;
    viewToProj = glm::perspective(fov, aspectRatio, zNear, zFar);
    worldToProj = viewToProj*worldToView;
}


void Camera::translate(const glm::vec3& trans) {
    cameraPosition += trans;
    glm::vec3 lookAt = cameraPosition + direction;
    worldToView = glm::lookAt(cameraPosition, lookAt, up);
    worldToProj = viewToProj*worldToView;
}


void Camera::rotate(float _pitch, float _yaw) {
    pitch = glm::max(-89.5f, glm::min(pitch + _pitch, 89.5f));
    yaw += _yaw;
    
    direction.x = glm::cos(glm::radians(pitch)) * glm::cos(glm::radians(yaw));
    direction.y = glm::cos(glm::radians(pitch)) * glm::sin(glm::radians(yaw));
    direction.z = glm::sin(glm::radians(pitch));
    direction = glm::normalize(direction);
    
    glm::vec3 lookAt = cameraPosition + direction;
    worldToView = glm::lookAt(cameraPosition, lookAt, up);
    worldToProj = viewToProj*worldToView;
}
