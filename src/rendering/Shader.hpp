#pragma once

#include <sys/types.h>
#include <string>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include "utils/Types.hpp"

/**
 * Texture unit id constants
 */
const int TEX_UNIT_DIFFUSE = 0;
const int TEX_UNIT_ZBUFFER = 1;
const int TEX_UNIT_VOLUMETRIC = 2;
// We reserve 10 texture units to regular textures (diffuse, normal, bump...)
// The rest can be used for shadow map textures
const int TEX_UNIT_SHADOW_MAP_BASE = 10;


/**
 * A class managing a OpenGL shader program. This contains a vertex and a fragment shader,
 * and possibly also a geometry shader.
 */
class Shader {
    uint shaderId = 0;

    /**
     * Compile a shader of a specific shader type.
     */
    uint compileShader(const std::string& fileName, uint shaderType);
public:
    
    /**
     * Compile and link this shader program. Must be called before any setUniform method
     * is called on this shader.
     */
    void initialize(const std::string& vertFileName, const std::string& fragFileName);
    void initialize(const std::string& vertFileName, const std::string& geomFileName,
                    const std::string& fragFileName);
    
    /**
     * Activate this shader program. Must be called before any setUniform methods are called.
     */
    void use();
    uint getShaderId() {return shaderId;}
    
    /**
     * Update the value of an uniform in this shader.
     */
    void setUniform1i(const std::string& name, int i);
    void setUniform1f(const std::string& name, float f);
    void setUniform3fv(const std::string& name, const glm::vec3& v);
    void setUniformMatrix4fv(const std::string& name, const glm::mat4& m);
};
