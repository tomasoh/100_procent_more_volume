
#include "glad/glad.h"

#include "rendering/RenderEngine.hpp"
#include "shape/Quad.hpp"

RenderEngine::RenderEngine(int _width, int _height) : width{_width}, 
                    height{_height}, volumetricFramebuffer(_width, _height),
                    hdrFramebuffer(_width, _height) {
                                    
    objectShader.initialize("shaders/default.vert", 
                            "shaders/default.frag");
    volumetricShader.initialize("shaders/volumetric.vert", 
                                "shaders/volumetric.frag");
    toneMappingShader.initialize("shaders/tone_mapping.vert", 
                                 "shaders/tone_mapping.frag");
    Light::initShaders();
    
    quadMesh = new Quad();
    
    // Set the texture units for the object shader
    objectShader.use();
    objectShader.setUniform1i("diffuseTex", TEX_UNIT_DIFFUSE);
    for(uint light = 0; light < MAX_LIGHTS; ++light) {
        int texUnit = light + TEX_UNIT_SHADOW_MAP_BASE;
        objectShader.setUniform1i("lights[" + std::to_string(light) + "].pointShadowMap", texUnit);
        objectShader.setUniform1i("lights[" + std::to_string(light) + "].spotShadowMap", texUnit + MAX_LIGHTS);
    }
    volumetricShader.use();
    volumetricShader.setUniform1i("zBuffer", TEX_UNIT_ZBUFFER);
    for(uint light = 0; light < MAX_LIGHTS; ++light) {
        int texUnit = light + TEX_UNIT_SHADOW_MAP_BASE;
        volumetricShader.setUniform1i("lights[" + std::to_string(light) + "].pointShadowMap", texUnit);
        volumetricShader.setUniform1i("lights[" + std::to_string(light) + "].spotShadowMap", texUnit + MAX_LIGHTS);
    }
    toneMappingShader.use();
    toneMappingShader.setUniform1i("hdrTexture", TEX_UNIT_DIFFUSE);
    toneMappingShader.setUniform1i("volumetricTexture", TEX_UNIT_VOLUMETRIC);

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glGenQueries(4, query);
}


RenderEngine& RenderEngine::getInstance() {
    // Creates a singleton instance of a RenderEngine.
    static RenderEngine instance;
    return instance;
}


RenderEngine::~RenderEngine() {
    delete quadMesh;
}


void RenderEngine::resize(int _width, int _height) {
    width = _width;
    height = _height;
    hdrFramebuffer.resize(width, height);
    volumetricFramebuffer.resize(width, height);
    glViewport(0, 0, width, height);
}    


void RenderEngine::render(Scene& scene, const Camera& camera) {

    // Update shadow maps
    glBeginQuery(GL_TIME_ELAPSED, query[0]);
    glEnable(GL_DEPTH_TEST);
    for(Light* light : scene.getLights()) {
        light->renderShadowMap(scene);
    }
    
    glViewport(0, 0, width, height);
    glEndQuery(GL_TIME_ELAPSED);
    
    // Draw scene to HDR frame buffer
    glBeginQuery(GL_TIME_ELAPSED, query[1]);
    hdrFramebuffer.use();
    glm::vec3 bgColor = scene.getBackgroundColor();
    glClearColor(bgColor.r, bgColor.g, bgColor.b, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    objectShader.use();
    objectShader.setUniform3fv("viewPos", camera.getPosition());
    objectShader.setUniform1f("absorptionTau", scene.getVolume()->absorptionTau);
    objectShader.setUniform3fv("absorptionColor", scene.getVolume()->absorptionColor);
    objectShader.setUniform1f("scatteringTau", scene.getVolume()->scatteringTau);
    objectShader.setUniform1i("volumeType", scene.getVolume()->volumeType);
    scene.uploadLights(objectShader);
    scene.drawScene(camera.getWorldToProj(), objectShader);
    glEndQuery(GL_TIME_ELAPSED);
    
    // Draw volumetric pass
    glBeginQuery(GL_TIME_ELAPSED, query[2]);
    glDisable(GL_DEPTH_TEST);
    volumetricFramebuffer.use();
    volumetricShader.use();
    glActiveTexture(GL_TEXTURE0 + TEX_UNIT_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, hdrFramebuffer.getColorBufferId());
    
    glActiveTexture(GL_TEXTURE0 + TEX_UNIT_ZBUFFER);
    glBindTexture(GL_TEXTURE_2D, hdrFramebuffer.getZBufferId());
    
    scene.uploadLights(volumetricShader);
    
    volumetricShader.setUniformMatrix4fv("projectionToWorld", camera.getProjToWorld());
    volumetricShader.setUniform3fv("cameraPosition", camera.getPosition());
    volumetricShader.setUniform1f("screenWidth", width);
    volumetricShader.setUniform1f("screenHeight", height);
    if(scene.getVolume()) {
        scene.getVolume()->uploadToShader(volumetricShader);
    }
    
    quadMesh->drawMesh(volumetricShader.getShaderId());
    glEndQuery(GL_TIME_ELAPSED);
    
    // Draw resulting frame buffer to screen with gamma correction and tone mapping
    glBeginQuery(GL_TIME_ELAPSED, query[3]);
    useScreenFrameBuffer();
    toneMappingShader.use();
    toneMappingShader.setUniform1i("volumeType", scene.getVolume()->volumeType);
    toneMappingShader.setUniform1i("interleavedSampling", scene.getVolume()->interleavedSampling);
    glActiveTexture(GL_TEXTURE0 + TEX_UNIT_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, hdrFramebuffer.getColorBufferId());

    glActiveTexture(GL_TEXTURE0 + TEX_UNIT_VOLUMETRIC);
    glBindTexture(GL_TEXTURE_2D, volumetricFramebuffer.getColorBufferId());
    
    postProcessingData.uploadToShader(toneMappingShader);
    
    quadMesh->drawMesh(toneMappingShader.getShaderId());   
    scene.resetDirty();
    glEndQuery(GL_TIME_ELAPSED);

    for(uint i = 0; i < 3; ++i) {
        GLuint64 elapsedTime;
        glGetQueryObjectui64v(query[i], GL_QUERY_RESULT, &elapsedTime);
        msTime[i] = elapsedTime / 1000000.0;
    }
    msTime[4] = msTime[0] + msTime[1] + msTime[2] + msTime[3];
}
