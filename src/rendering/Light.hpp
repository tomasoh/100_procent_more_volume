#pragma once

#include <glm/vec3.hpp>

#include "rendering/Shader.hpp"
#include "scene/Scene.hpp"

class Scene;

// Keep this in sync with max value in shader!
const int MAX_LIGHTS = 8;

const int POINT_LIGHT_SHADOW_MAP_SIZE = 1024;
const int SPOT_LIGHT_SHADOW_MAP_SIZE = 2048;
const float SHADOW_Z_NEAR = 0.01f;
const float SHADOW_Z_FAR = 100.0f;


enum LightType {
    POINT_LIGHT,
    SPOT_LIGHT
};

/**
 * Light class represents a light, either a spot light or a point light. The class takes care of 
 * shadow maps and uploading the light to a shader.
 */
class Light {
    LightType type;
    glm::vec3 position;
    glm::vec3 direction; // For spot lights
    float cutOffAngle; // For spot ligths, angle of spot cone in degrees from normal
    glm::vec3 color;
    float intensity;
    glm::mat4 worldToProj; // For spot lights
    
    int shadowMapSize;
    uint shadowMapId;
    uint shadowFBOId;
    
    // Flag to check if the attributes of this light has changed in a way
    // that requires the shadow map to be rerendered.
    bool dirtyShadowMap = true;
    
    void initShadowMap();
    
public:
    // Constructor for Point light source
    Light(const glm::vec3& _pos, const glm::vec3& _color, float _intensity);
    // Constructor for Spot light source
    Light(const glm::vec3& _pos, const glm::vec3& _dir, float _cutOffAngle, const glm::vec3& _color, float _intensity);
    ~Light();
    
    /** 
     * Compile the shadow map shaders, must be called before the 
     * lights can be used.
     */
    static void initShaders();
    
    /**
     * Render the scene to this light's shadow map texture.
     */
    void renderShadowMap(Scene& scene);
    
    /**
     * Upload this light to a shader. Variable is the variable name of the 
     * light struct (array) in the shader program and texUnit is an index
     * for the texture unit to use for the shadow map.
     */
    void uploadToShader(Shader& shader, const std::string& variable, 
                        int texUnit);

    void updateLightPosition(const glm::vec3& newPosition);
    void updateLightDirection(const glm::vec3& newDirection);
    void updateSpotConeAngle(float newCutOffAngle);
    void updateLightColor(const glm::vec3& newColor);
    
    uint getShadowMapTexture() {return shadowMapId;}
    uint getShadowMapFBO() {return shadowFBOId;}
};
