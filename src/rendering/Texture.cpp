
#include "glad/glad.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "rendering/Texture.hpp"
#include "utils/Types.hpp"



uint loadTexture(const std::string& filename) {
    const auto& it = loadedTextures.find(filename);
    if(it != loadedTextures.end()) {
        return it->second;
    }
    
    uint textureId;
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    int width, height;
    int components;
    unsigned char* data = stbi_load(filename.c_str(), &width, &height, &components, 0);
    if(components == 3)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    else if(components == 4)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(data);
    
    loadedTextures.insert({filename, textureId});
    return textureId;
}
