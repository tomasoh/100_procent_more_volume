#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

const glm::vec3 Z_AXIS = glm::vec3(0.0f, 0.0f, 1.0f);

/**
 * The Camera class handles a perspective camera's settings.
 */
class Camera {
    glm::mat4 worldToView;
    glm::mat4 viewToProj;
    glm::mat4 worldToProj;
    
    glm::vec3 cameraPosition;
    glm::vec3 direction;
    glm::vec3 up;
    
    float pitch = 0.0;
    float yaw = 0.0;
    
    float fov;
    float aspectRatio;
    float zNear;
    float zFar;
    
public:
    Camera() : aspectRatio{1.0} {};
    Camera(float _aspectRatio) : aspectRatio{_aspectRatio} {};
    ~Camera() = default;
    
    
    /**
     * Update the orientation of this camera.
     */
    void updateOrientation(const glm::vec3& _cameraPosition,
                      const glm::vec3& _direction,
                      const glm::vec3& _up = Z_AXIS);
    
    /**
     * Update the projection of this camera.
     *  fov:    Field of view in degrees
     *  width:  The width of the rendered frame
     *  height: The height of the rendered frame
     *  zNear:  The near depth limit of the projection
     *  zFar:   The far depth limit of the projection
     */
    void updateProjection(float _fov, float _zNear, float _zFar);
    
    /**
     * Update the aspect ratio of this camera.
     */
    void updateAspectRatio(float _aspectRatio);
    
    void translate(const glm::vec3& trans);
    void rotate(float _pitch, float _yaw);
    
    const glm::mat4 getWorldToProj() const {return worldToProj;}
    const glm::mat4 getProjToWorld() const {return glm::inverse(worldToProj);}
    const glm::vec3 getPosition() const {return cameraPosition;}
    const glm::vec3 getDirection() const {return direction;}
    float getZNear() const {return zNear;}
    float getZFar() const {return zFar;}
};
