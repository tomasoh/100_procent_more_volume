#pragma once

#include <glm/vec3.hpp>

struct HomogeneousVolume {
    enum VolumeType {
        NONE = 0,
        DISTANCE_FOG = 1,
        ABSORPTION = 2,
        SCATTERING = 3
    };

    enum PhaseFunctionType {
        HENYEY_GREENSTEIN = 0,
        MIE_HAZY = 1,
        MIE_MURKY = 2,
        RAYLEIGH = 3
    };
    
    VolumeType volumeType = NONE;
    PhaseFunctionType phaseFunctionType = HENYEY_GREENSTEIN;
    
    // For distance fog
    float fogNear = 0.0f;
    float fogFar = 100.0f;
    glm::vec3 fogColor = {0.5f, 0.5f, 0.5f};
    
    // For absorption
    float absorptionTau = 0.03f; 
    glm::vec3 absorptionColor = {0.5f, 0.5f, 0.5f};
    
    // For scattering
    int scatteringSamples = 50;
    float scatteringTau = 0.3f;
    float anisotropy = 0.0f;
    float scatteringZFar = 30.0f;
    glm::vec3 scatteringColor = {1.0f, 1.0f, 1.0f};
    int interleavedSampling = 0;
    
    void uploadToShader(Shader& volumeShader) {
        volumeShader.setUniform1i("volumeType", volumeType);
        volumeShader.setUniform1i("phaseFunctionType", phaseFunctionType);
        
        volumeShader.setUniform1f("fogNear", fogNear);
        volumeShader.setUniform1f("fogFar", fogFar);
        volumeShader.setUniform3fv("fogColor", fogColor);
        
        volumeShader.setUniform1f("absorptionTau", absorptionTau);
        volumeShader.setUniform3fv("absorptionColor", absorptionColor);
        
        volumeShader.setUniform1i("scatteringSamples", scatteringSamples);
        volumeShader.setUniform1f("scatteringTau", scatteringTau);
        volumeShader.setUniform1f("anisotropy", anisotropy);
        volumeShader.setUniform1f("scatteringZFar", scatteringZFar);
        volumeShader.setUniform3fv("scatteringColor", scatteringColor);
    }
};
