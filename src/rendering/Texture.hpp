#pragma once

#include <unordered_map>

#include "utils/Types.hpp"

static std::unordered_map<std::string, uint> loadedTextures;


/**
 * Returns the OpenGL object id of a loaded texture based on the file name. 
 * If the file is not loaded, the texture will be loaded from disk. If the 
 * texture is already loaded, the existing id will be returned.
 */
uint loadTexture(const std::string& filename);
