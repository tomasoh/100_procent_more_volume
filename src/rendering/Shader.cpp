#include "Shader.hpp"

#include <glad/glad.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <glm/gtc/type_ptr.hpp>

// Uncomment this to print error messages to stdout when a uniform is not found in the shader.
// If not used, the shader will ignore unknown uniforms.
// #define DEBUG

const uint MAX_LOG_LENGTH = 1024;


uint Shader::compileShader(const std::string& fileName, uint shaderType) {
    std::ifstream fileDataStream;
    std::stringstream shaderStringStream;
    fileDataStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    
    try {
        fileDataStream.open(fileName);
        shaderStringStream << fileDataStream.rdbuf();
    } catch(std::ifstream::failure e) {
        std::cerr << "Error reading shader from file: " << std::endl;
        std::cerr << fileName << "." << std::endl;
    }
    fileDataStream.close();
    
    std::string shaderStr = shaderStringStream.str();
    const char* shaderCode = shaderStr.c_str();
    
    uint shaderStageId;
    int success;
    char infoLog[MAX_LOG_LENGTH];

    // Compile the shader
    shaderStageId = glCreateShader(shaderType);
    glShaderSource(shaderStageId, 1, &shaderCode, nullptr);
    glCompileShader(shaderStageId);
    glGetShaderiv(shaderStageId, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(shaderStageId, MAX_LOG_LENGTH, nullptr, infoLog);
        std::cerr << "ERROR: Shader compilation failed for shader ";
        std::cerr << fileName << ":" << std::endl;
        std::cerr << infoLog << std::endl;
        exit(1);
    }
    
    return shaderStageId;
}


void Shader::initialize(const std::string& vertFileName, const std::string& fragFileName) {
    uint vertShaderId = compileShader(vertFileName, GL_VERTEX_SHADER);
    uint fragShaderId = compileShader(fragFileName, GL_FRAGMENT_SHADER);
    
    shaderId = glCreateProgram();
    glAttachShader(shaderId, vertShaderId);
    glAttachShader(shaderId, fragShaderId);
    glLinkProgram(shaderId);
    
    int success;
    char infoLog[MAX_LOG_LENGTH];
    glGetProgramiv(shaderId, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(shaderId, MAX_LOG_LENGTH, nullptr, infoLog);
        std::cerr << "ERROR: Shader linking failed:" << std::endl;
        std::cerr << infoLog << std::endl;
        exit(1);
    }
    
    // Delete the shaders as they're linked into our program now
    glDeleteShader(vertShaderId);
    glDeleteShader(fragShaderId);
}


void Shader::initialize(const std::string& vertFileName, const std::string& geomFileName,
                    const std::string& fragFileName) {
    uint vertShaderId = compileShader(vertFileName, GL_VERTEX_SHADER);
    uint geomShaderId = compileShader(geomFileName, GL_GEOMETRY_SHADER);
    uint fragShaderId = compileShader(fragFileName, GL_FRAGMENT_SHADER);
    
    shaderId = glCreateProgram();
    glAttachShader(shaderId, vertShaderId);
    glAttachShader(shaderId, geomShaderId);
    glAttachShader(shaderId, fragShaderId);
    glLinkProgram(shaderId);
    
    int success;
    char infoLog[MAX_LOG_LENGTH];
    glGetProgramiv(shaderId, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(shaderId, MAX_LOG_LENGTH, nullptr, infoLog);
        std::cerr << "ERROR: Shader linking failed:" << std::endl;
        std::cerr << infoLog << std::endl;
        exit(1);
    }
    
    // Delete the shaders as they're linked into our program now
    glDeleteShader(vertShaderId);
    glDeleteShader(geomShaderId);
    glDeleteShader(fragShaderId);
}


void Shader::use() {
    glUseProgram(shaderId);
}

void Shader::setUniform1i(const std::string& name, int i) {
        int loc = glGetUniformLocation(shaderId, name.c_str());
#ifdef DEBUG
    if(loc == -1) {
        std::cerr << "Could not find uniform named '" << name << "'" << std::endl;
    }
#endif
    glUniform1i(loc, i);
}

void Shader::setUniform1f(const std::string& name, float f) {
        int loc = glGetUniformLocation(shaderId, name.c_str());
#ifdef DEBUG
    if(loc == -1) {
        std::cerr << "Could not find uniform named '" << name << "'" << std::endl;
    }
#endif
    glUniform1f(loc, f);
}

void Shader::setUniform3fv(const std::string& name, const glm::vec3& v) {
    int loc = glGetUniformLocation(shaderId, name.c_str());
#ifdef DEBUG
    if(loc == -1) {
        std::cerr << "Could not find uniform named '" << name << "'" << std::endl;
    }
#endif
    glUniform3fv(loc, 1, glm::value_ptr(v));
}

void Shader::setUniformMatrix4fv(const std::string& name, const glm::mat4& m) {
    int loc = glGetUniformLocation(shaderId, name.c_str());
#ifdef DEBUG
    if(loc == -1) {
        std::cerr << "Could not find uniform named '" << name << "'" << std::endl;
    }
#endif
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m));
}
