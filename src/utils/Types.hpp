#pragma once

// Fix some typdefs when compiling with MS Visual Studio
// This will let us use 'not', 'and' 'or'... keywords
#ifdef _MSC_VER
# include <iso646.h>
typedef unsigned int uint;
#endif