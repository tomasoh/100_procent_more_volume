
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>

#include "utils/ObjLoader.hpp"
#include "utils/Types.hpp"


/**
 * Import an .obj file and return a mesh object with it.
 * The importer handles normals and texture coordinates.
 * 
 * Note: the importer only supports importing trinagles and quads, 
 *      not arbitrary n-gons.
 */
Mesh* loadObj(const std::string& filename) {
    // Map of every unique vertex,uv,normal combination
    std::unordered_map<std::string, uint> vertexMap;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> uvs;
    std::vector<uint> indecies;
    uint counter = 0;

    std::ifstream stream(filename);
    if(!stream.is_open()) {
        std::cerr << "ERROR: Failed to open file " << filename << std::endl;
        return nullptr;
    }

    std::string line;
    while(std::getline(stream, line)) {
        if(line[0] == '#') // Comment, ignore
            continue;
        
        std::stringstream ss(line);
        std::string type;
        ss >> type;
        if(type == "v") {
            float x, y, z;
            ss >> x >> y >> z;
            vertices.push_back({x, y, z});
        }
        else if(type == "vt") {
            float u, v;
            ss >> u >> v;
        v = 1.0f - v; //OpenGL's v-coordinate is flipped compared to image file
            uvs.push_back({u, v});
        }
        else if(type == "vn") {
            float x, y, z;
            ss >> x >> y >> z;
            normals.push_back({x, y, z});
        }
        else if(type == "f") {
            std::string vertexCode; // like "v" or "v/vt" or "v/vt/vn" or "v//vn" 
            for(uint i = 0; i < 3; ++i) {
                ss >> vertexCode;
                uint id = counter;
                
                const auto &v = vertexMap.find(vertexCode);
                if(v == end(vertexMap)) {
                    vertexMap[vertexCode] = counter;
                    counter++;
                }
                else {
                    id = vertexMap[vertexCode];
                }
                indecies.push_back(id);
            }
            
            if(ss >> vertexCode) {
                // This is a quad, triangle 0->1->2 already added, now we 
                // add triangle 2->3->0
                uint vert0 = indecies[indecies.size()-3];
                indecies.push_back(indecies.back());
                
                uint id = counter;
                const auto &v = vertexMap.find(vertexCode);
                if(v == end(vertexMap)) {
                    vertexMap[vertexCode] = counter;
                    counter++;
                }
                else {
                    id = vertexMap[vertexCode];
                }
                indecies.push_back(id);
                indecies.push_back(vert0);
            }
        }
    }

    // Reorganize to a format for the GPU buffers
    std::vector<glm::vec3> vertexArray;
    std::vector<glm::vec2> uvArray;
    std::vector<glm::vec3> normalArray;
    
    vertexArray.resize(vertexMap.size());
    if(not uvs.empty())
        uvArray.resize(vertexMap.size());
    if(not normals.empty())
        normalArray.resize(vertexMap.size());
    

    for(const auto &vert : vertexMap) {
        // Process one corner of a triangle of obj definition, like:
        // "1"   or   "1/2"    or   "1//2"    or   "1/2/3"
        std::stringstream ss(vert.first);
        uint vertexId = vert.second;
        
        uint tmp;
        ss >> tmp;
        vertexArray[vertexId] = vertices[tmp-1]; // .obj is 1 indexed
        if(ss.peek() == '/') {
            ss.ignore();
            if(ss.peek() == '/') {
                ss.ignore();
                ss >> tmp;
                normalArray[vertexId] = normals[tmp-1];
            }
            else {
                ss >> tmp;
                uvArray[vertexId] = uvs[tmp-1];
                
                if(ss.peek() == '/') {
                    ss.ignore();
                    ss >> tmp;
                    normalArray[vertexId] = normals[tmp-1];
                }
            }
        }
    }
    Mesh* mesh = new Mesh(vertexArray, uvArray, normalArray, indecies);
    return mesh;
}
