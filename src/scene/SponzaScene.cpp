
#include <glm/vec3.hpp>
#include <glm/gtx/transform.hpp>

#include "rendering/Texture.hpp"
#include "scene/Scene.hpp"


SponzaScene::SponzaScene() : Scene() {
    Mesh* arch = loadMesh("res/sponza/sponza_arch.obj");
    Mesh* bricks = loadMesh("res/sponza/sponza_bricks.obj");
    Mesh* ceiling = loadMesh("res/sponza/sponza_ceiling.obj");
    Mesh* columnA = loadMesh("res/sponza/sponza_column_a.obj");
    Mesh* columnB = loadMesh("res/sponza/sponza_column_b.obj");
    Mesh* columnC = loadMesh("res/sponza/sponza_column_c.obj");
    Mesh* details = loadMesh("res/sponza/sponza_details.obj");
    Mesh* floor = loadMesh("res/sponza/sponza_floor.obj");
    Mesh* roof = loadMesh("res/sponza/sponza_roof.obj");
    Mesh* fabricBlue = loadMesh("res/sponza/sponza_fabric_blue.obj");
    Mesh* fabricGreen = loadMesh("res/sponza/sponza_fabric_green.obj");
    Mesh* fabricRed = loadMesh("res/sponza/sponza_fabric_red.obj");

    MeshInstance* archInstance = new MeshInstance(arch, glm::mat4(1.0f));
    MeshInstance* bricksInstance = new MeshInstance(bricks, glm::mat4(1.0f));
    MeshInstance* ceilingInstance = new MeshInstance(ceiling, glm::mat4(1.0f));
    MeshInstance* columnAInstance = new MeshInstance(columnA, glm::mat4(1.0f));
    MeshInstance* columnBInstance = new MeshInstance(columnB, glm::mat4(1.0f));
    MeshInstance* columnCInstance = new MeshInstance(columnC, glm::mat4(1.0f));
    MeshInstance* detailsInstance = new MeshInstance(details, glm::mat4(1.0f));
    MeshInstance* floorInstance = new MeshInstance(floor, glm::mat4(1.0f));
    MeshInstance* roofInstance = new MeshInstance(roof, glm::mat4(1.0f));
    MeshInstance* blueInstance = new MeshInstance(fabricBlue, glm::mat4(1.0f));
    MeshInstance* greenInstance = new MeshInstance(fabricGreen, glm::mat4(1.0f));
    MeshInstance* redInstance = new MeshInstance(fabricRed, glm::mat4(1.0f));

    Material material;
    material.diffuseColor = glm::vec3(0.3f, 0.75f, 0.1f);
    material.diffuseFactor = 1.0f;
    material.specularFactor = 1.0f;
    material.specularSharpness = 32.0f;
    material.ambientFactor = 0.02f;

    archInstance->setMaterial(material);
    bricksInstance->setMaterial(material);
    ceilingInstance->setMaterial(material);
    columnAInstance->setMaterial(material);
    columnBInstance->setMaterial(material);
    columnCInstance->setMaterial(material);
    detailsInstance->setMaterial(material);
    floorInstance->setMaterial(material);
    roofInstance->setMaterial(material);
    blueInstance->setMaterial(material);
    greenInstance->setMaterial(material);
    redInstance->setMaterial(material);


    archInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_arch_diff.tga"));
    bricksInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_bricks_a_diff.tga"));
    ceilingInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_ceiling_a_diff.tga"));
    columnAInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_column_a_diff.tga"));
    columnBInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_column_b_diff.tga"));
    columnCInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_column_c_diff.tga"));
    detailsInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_details_diff.tga"));
    floorInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_floor_a_diff.tga"));
    roofInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_roof_diff.tga"));
    blueInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_fabric_blue_diff.tga"));
    greenInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_fabric_green_diff.tga"));
    redInstance->setDiffuseTexture(loadTexture("res/sponza/sponza_fabric_red_diff.tga"));

    addObject(archInstance);
    addObject(bricksInstance);
    addObject(ceilingInstance);
    addObject(columnAInstance);
    addObject(columnBInstance);
    addObject(columnCInstance);
    addObject(detailsInstance);
    addObject(floorInstance);
    addObject(roofInstance);
    addObject(blueInstance);
    addObject(greenInstance);
    addObject(redInstance);

    Light* light = new Light(glm::vec3(0.0f, 0.0f, 8.0f), 
                                glm::vec3(1.0f, 0.8f, 0.6f), 800.0f);
    addLight(light);
    getVolume()->scatteringTau = 0.2f;
}


void SponzaScene::animate(double deltaT) {
    // Do nothing
}


Camera SponzaScene::getDefaultCamera() {
    Camera camera;
    camera.updateOrientation({10.0f, 0.0f, 1.5f}, { -1.0f, 0.0f, 0.0f});
    camera.updateProjection(45.0f, 0.1f, 100.0f);
    return camera;
}


glm::vec3 SponzaScene::getBackgroundColor() {
    return glm::vec3(0.648f, 0.754f, 1.0f);
}