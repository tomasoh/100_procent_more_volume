#include "Scene.hpp"

#include <glad/glad.h>
#include <glm/mat4x4.hpp>

#include "utils/ObjLoader.hpp"


Scene::Scene() {
    volume = new HomogeneousVolume();
}


Scene::~Scene() {
    for(Mesh* mesh : loadedMeshes) {
        delete mesh;
    }
    
    for(MeshInstance* instance : objects) {
        delete instance;
    }
    
    for(Light* light: lights) {
        delete light;
    }
    delete volume;
}


Mesh* Scene::loadMesh(const std::string& filename) {
    Mesh* mesh = loadObj(filename);
    loadedMeshes.push_back(mesh);
    return mesh;
}


void Scene::addObject(MeshInstance* instance) {
    objects.push_back(instance);
}


void Scene::addLight(Light* light) {
    lights.push_back(light);
}

    
void Scene::uploadLights(Shader& objectShader) {
    // Set all lights' uniforms in the shader
    objectShader.setUniform1i("numLights", lights.size());
    for(uint i = 0; i < lights.size(); ++i) {
        std::string variable = "lights[" + std::to_string(i) + "]";
        int texUnit = i + TEX_UNIT_SHADOW_MAP_BASE;
        lights[i]->uploadToShader(objectShader, variable, texUnit);
    }
}


void Scene::drawScene(const glm::mat4& worldToProj, Shader& objectShader) {
    for(MeshInstance* instance : objects) {
        instance->drawInstance(worldToProj, objectShader);
    }
}


void Scene::drawSceneToShadowMap(Shader& shader) {
    for(MeshInstance* instance : objects) {
        instance->drawInstanceAsShadow(shader);
    }
}
