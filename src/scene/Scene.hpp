#pragma once

#include <vector>
#include <glm/mat4x4.hpp>

#include <rendering/Camera.hpp>
#include <rendering/Light.hpp>
#include <rendering/Volume.hpp>
#include <shape/Mesh.hpp>

class Light;

enum SceneType {
    SCENE_FLOATING_CUBES,
    SCENE_SPONZA,
    SCENE_SIBENIK,
    SCENE_SPOT
};

/**
 * A Scene is an abstract representation of a 3D world containing 
 * several objects(meshes) and lights. These objects and lights might be 
 * animated by the Scene. This class is intended to be inherited and 
 * implemented by the specific scenes.
 */
class Scene {
    std::vector<Mesh*> loadedMeshes;
    std::vector<MeshInstance*> objects;
    std::vector<Light*> lights;
    HomogeneousVolume* volume;
    
    bool dirty = true;
    
    /**
     * Updates the lighting data uniforms in the active shader. Also 
     * recalculates the shadow maps for all lights in the scene if needed.
     */
    void updateLights(Shader& objectShader);
    
protected:
    /**
     * Loads a mesh from file. Returns a pointer to the loaded mesh.
     * This scene takes care of deleting the mesh object once the scene is
     * deleted.
     */
    Mesh* loadMesh(const std::string& filename);
    
public:
    Scene();
    virtual ~Scene();
    
    /**
     * Adds an object instance to this scene. Takes ownership over the pointer.
     */
    void addObject(MeshInstance* obj);
    
    
    /**
     * Adds a light to this scene. Takes ownership over the pointer.
     */
    void addLight(Light* light);
    std::vector<Light*>& getLights() {return lights;}
    void uploadLights(Shader& objectShader);
    
    /**
     * Getter for the homogeneous volume.
     */
    HomogeneousVolume* getVolume() {return volume;}
    
    /**
     * Animates this scene deltaT seconds forward.
     * Note: if the scene is animated in such a way that the light's shadow maps must be
     * rerendered, the dirty flag or the scene must be set by calling Scene::setDirty().
     */
    virtual void animate(double /*deltaT*/) { /* Not implemented here */ };
    
    
    virtual Camera getDefaultCamera() { return {}; }

    virtual glm::vec3 getBackgroundColor() { return glm::vec3(0.0f, 0.0f, 0.0f); }
    
    /**
     * Draw all objects in this scene to the active buffer as seen by the
     * camera defined by the worldToProj matrix.
     */
    void drawScene(const glm::mat4& worldToProj, Shader& objectShader);
    
    /**
     * Draw the scene to a shadow map. Uses the currently active shader.
     */
    void drawSceneToShadowMap(Shader& shader);
    
    /**
     * Check if the scene is dirty, i.e. that some mesh in the scene has 
     * moved since last frame.
     */
    bool isDirty() {return dirty;}
    void setDirty() {dirty = true;}
    void resetDirty() {dirty = false;}
};


class FloatingCubesScene : public Scene {
    MeshInstance* cubes;
public:
    FloatingCubesScene();
    ~FloatingCubesScene();
    
    void animate(double deltaT) override;
    Camera getDefaultCamera() override;
    glm::vec3 getBackgroundColor() override;
};


class SponzaScene : public Scene {
public:
    SponzaScene();
    ~SponzaScene() = default;

    void animate(double deltaT) override;
    Camera getDefaultCamera() override;
    glm::vec3 getBackgroundColor() override;
};



class SibenikScene : public Scene {
public:
    SibenikScene();
    ~SibenikScene() = default;

    void animate(double deltaT) override;
    Camera getDefaultCamera() override;
    glm::vec3 getBackgroundColor() override;
};


class SpotScene : public Scene {
public:
    SpotScene();
    ~SpotScene() = default;

    void animate(double deltaT) override;
    Camera getDefaultCamera() override;
    glm::vec3 getBackgroundColor() override;
};

