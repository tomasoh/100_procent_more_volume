
#include <glm/vec3.hpp>
#include <glm/gtx/transform.hpp>

#include "rendering/Texture.hpp"
#include "scene/Scene.hpp"

const float ROTATION_SPEED = 0.3f;

FloatingCubesScene::FloatingCubesScene() : Scene() {
    Mesh* mesh = loadMesh("res/floating_cubes.obj");
    
    cubes = new MeshInstance(mesh, glm::mat4(1.0f));
    
    Material material;
    material.diffuseColor = glm::vec3(0.3f, 0.75f, 0.1f);
    material.diffuseFactor = 1.0f;
    material.specularFactor = 1.0f;
    material.specularSharpness = 32.0f;
    material.ambientFactor = 0.01f;
    
    cubes->setMaterial(material);
    uint texture = loadTexture("res/gray.png");
    cubes->setDiffuseTexture(texture);
    addObject(cubes);
    
    Light* light = new Light(glm::vec3(-6.0f, -12.0f, 8.0f), 
                             glm::vec3(0.0f, 3.0f, -1.0f), 30.0f, 
                             glm::vec3(0.5f, 0.5f, 1.0f), 1600.0f);
    addLight(light);
    getVolume()->scatteringTau = 0.07f;
    getVolume()->scatteringZFar = 42.0f;
    getVolume()->absorptionColor = glm::vec3(0.43f, 0.49f, 0.55f);
}

FloatingCubesScene::~FloatingCubesScene() = default;

void FloatingCubesScene::animate(double deltaT) {
    cubes->applyTransformation(glm::rotate(float(ROTATION_SPEED*deltaT), 
                                           glm::vec3(0.0f, 0.0f, 1.0f)));

    setDirty(); // set flag to make shadow maps rerender.
}


Camera FloatingCubesScene::getDefaultCamera() {
    Camera camera;
    camera.updateOrientation({-20.0f, -10.0f, 10.0f}, {20.0f, 10.0f, -10.0f});
    camera.updateProjection(45.0f, 0.1f, 100.0f);
    return camera;
}


glm::vec3 FloatingCubesScene::getBackgroundColor() {
    return glm::vec3(0.0f, 0.0f, 0.0f);
}