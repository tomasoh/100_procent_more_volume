
#include <glm/vec3.hpp>
#include <glm/gtx/transform.hpp>

#include "rendering/Texture.hpp"
#include "scene/Scene.hpp"


SibenikScene::SibenikScene() : Scene() {
    Mesh* wallsLimestone = loadMesh("res/sibenik/wall_limestone.obj");
    Mesh* wallsStone = loadMesh("res/sibenik/wall_stone.obj");
    Mesh* floorRed = loadMesh("res/sibenik/floor_red.obj");
    Mesh* floorMarble = loadMesh("res/sibenik/floor_marble.obj");
    Mesh* details = loadMesh("res/sibenik/details.obj");

    MeshInstance* wallsLimestoneInstance = new MeshInstance(wallsLimestone, glm::mat4(1.0f));
    MeshInstance* wallsStoneInstance = new MeshInstance(wallsStone, glm::mat4(1.0f));
    MeshInstance* floorRedInstance = new MeshInstance(floorRed, glm::mat4(1.0f));
    MeshInstance* floorMarbleInstance = new MeshInstance(floorMarble, glm::mat4(1.0f));
    MeshInstance* detailsInstance = new MeshInstance(details, glm::mat4(1.0f));

    Material material;
    material.diffuseColor = glm::vec3(0.3f, 0.75f, 0.1f);
    material.diffuseFactor = 1.0f;
    material.specularFactor = 0.5f;
    material.specularSharpness = 32.0f;
    material.ambientFactor = 0.005f;

    wallsLimestoneInstance->setMaterial(material);
    wallsStoneInstance->setMaterial(material);
    floorRedInstance->setMaterial(material);
    floorMarbleInstance->setMaterial(material);
    detailsInstance->setMaterial(material);


    wallsLimestoneInstance->setDiffuseTexture(loadTexture("res/sibenik/limestone.tga"));
    uint stoneTexture = loadTexture("res/sibenik/stone.tga");
    wallsStoneInstance->setDiffuseTexture(stoneTexture);
    floorRedInstance->setDiffuseTexture(loadTexture("res/sibenik/TexturesCom_MarbleBase0095_1_seamless_S.tga"));
    floorMarbleInstance->setDiffuseTexture(loadTexture("res/sibenik/marble.tga"));
    detailsInstance->setDiffuseTexture(stoneTexture);

    addObject(wallsLimestoneInstance);
    addObject(wallsStoneInstance);
    addObject(floorRedInstance);
    addObject(floorMarbleInstance);
    addObject(detailsInstance);

    Light* light = new Light(glm::vec3(32.0f, 0.0f, 16.0f),
                            glm::vec3(-1.5f, 0.0f, -1.0f), 16.0f,
                            glm::vec3(1.0f, 1.0f, 1.0f), 1600.0f);
    Light* light2 = new Light(glm::vec3(32.0f, 0.0f, 20.0f),
                            glm::vec3(-1.5f, 0.0f, -1.0f), 10.0f,
                            glm::vec3(1.0f, 1.0f, 1.0f), 1600.0f);

    addLight(light);
    addLight(light2);

    getVolume()->absorptionTau = 0.02f;
    getVolume()->scatteringTau = 0.06f;
    getVolume()->scatteringZFar = 40.0f;
}


void SibenikScene::animate(double deltaT) {
    // Do nothing
}


Camera SibenikScene::getDefaultCamera() {
    Camera camera;
    camera.updateOrientation({-4.0f, 0.0f, 4.0f}, { 1.0f, 0.0f, 0.0f});
    camera.updateProjection(45.0f, 0.1f, 100.0f);
    return camera;
}


glm::vec3 SibenikScene::getBackgroundColor() {
    return glm::vec3(0.127f, 0.151f, 0.233f);
}