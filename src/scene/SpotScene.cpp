
#include <glm/vec3.hpp>
#include <glm/gtx/transform.hpp>

#include "rendering/Texture.hpp"
#include "scene/Scene.hpp"


SpotScene::SpotScene() : Scene() {
    Mesh* mesh = loadMesh("res/simple_sphere.obj");
    
    MeshInstance* sphere = new MeshInstance(mesh, glm::mat4(1.0f));
    
    Material material;
    material.diffuseColor = glm::vec3(0.3f, 0.3f, 0.3f);
    material.diffuseFactor = 1.0f;
    material.specularFactor = 1.0f;
    material.specularSharpness = 32.0f;
    material.ambientFactor = 0.01f;
    
    sphere->setMaterial(material);
    uint texture = loadTexture("res/gray.png");
    sphere->setDiffuseTexture(texture);
    addObject(sphere);
    
    Light* light = new Light(glm::vec3(-4.8f, 2.4f, 4.5f), 
                             glm::vec3(4.0f, -3.0f, -4.0f), 12.5f, 
                             glm::vec3(0.2f, 0.2f, 1.0f), 1600.0f);

    Light* light2 = new Light(glm::vec3(-0.8f, 3.4f, 4.4f),
                             glm::vec3(1.0f, -4.0f, -4.0f), 12.5f,
                             glm::vec3(1.0f, 0.5f, 0.2f), 1600.0f);

    Light* light3 = new Light(glm::vec3(4.4f, 2.4f, 4.3f),
                             glm::vec3(-4.5f, -2.5f, -4.0f), 12.5f,
                             glm::vec3(0.2f, 0.2f, 1.0f), 1600.0f);

    addLight(light);
    addLight(light2);
    addLight(light3);
    getVolume()->scatteringTau = 0.07f;
}


void SpotScene::animate(double deltaT) {
    // Do nothing
}


Camera SpotScene::getDefaultCamera() {
    Camera camera;
    camera.updateOrientation({0.0f, -13.0f, -1.0f}, {0.0f, 1.0f, 0.0f});
    camera.updateProjection(45.0f, 0.1f, 100.0f);
    return camera;
}


glm::vec3 SpotScene::getBackgroundColor() {
    return glm::vec3(0.0f, 0.0f, 0.0f);
}