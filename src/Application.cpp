#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Application.hpp"


Application& Application::getInstance() {
    // Creates a singleton instance of Application
    static Application app;
    return app;
}

void Application::init(SceneType sceneType) {
    if(!glfwInit()) {
        std::cerr << "GLFW initialization failed" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    int windowWidth = mode->width;
    int windowHeight = mode->height;

    window = glfwCreateWindow(windowWidth, windowHeight, "100% More Volume", 
                                glfwGetPrimaryMonitor(), nullptr);
    if(!window) {
        std::cerr << "Could not create GLFW window" << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    
    glfwSetMouseButtonCallback(window, Application::onGLFWMouseButtonEvent);
    glfwSetCursorPosCallback(window, Application::onGLFWMousePositionEvent);
    glfwSetKeyCallback(window, Application::onGLFWKeyEvent);
    glfwSetWindowSizeCallback(window, Application::onGLFWWindowResizeEvent);
    
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    
     if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cerr << "Could not initialize GLAD" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    engine = &RenderEngine::getInstance();
    engine->resize(windowWidth, windowHeight);

    if(sceneType == SCENE_FLOATING_CUBES) {
        scene = new FloatingCubesScene();
    }
    else if(sceneType == SCENE_SPONZA) {
        scene = new SponzaScene();
    }
    else if(sceneType == SCENE_SIBENIK) {
        scene = new SibenikScene();
    } 
    else if(sceneType == SCENE_SPOT) {
        scene = new SpotScene();
    }

    camera = scene->getDefaultCamera();
    camera.updateAspectRatio((float)windowWidth / (float)windowHeight);
    
    TwInit(TW_OPENGL_CORE, nullptr);
    TwWindowSize(windowWidth, windowHeight);
    tweakBar = TwNewBar("Main");
    TwDefine(" Main size='300 450' ");
    TwDefine(" Main valueswidth=fit ");
    TwDefine(" Main label='Volumetrics' refresh=0.5");
    
    HomogeneousVolume* vol = scene->getVolume();
    TwEnumVal volumeTypeData[] = { 
                                { HomogeneousVolume::NONE, "Off"}, 
                                { HomogeneousVolume::DISTANCE_FOG,  "Distance Fog" }, 
                                { HomogeneousVolume::ABSORPTION, "Absorption" },
                                { HomogeneousVolume::SCATTERING, "Scattering" } 
    };
    TwType volumeType = TwDefineEnum("Volume type", volumeTypeData, 4);

    TwEnumVal phaseFunctionTypeData[] = { 
                                { HomogeneousVolume::HENYEY_GREENSTEIN, "Henyey-Greenstein" },
                                { HomogeneousVolume::MIE_HAZY, "Mie Hazy (light fog)" },
                                { HomogeneousVolume::MIE_MURKY, "Mie Murky (dense fog)" },
                                { HomogeneousVolume::RAYLEIGH, "Rayleigh" } 
    };
    TwType phaseFunctionType = TwDefineEnum("Phase function", phaseFunctionTypeData, 4);

    TwEnumVal interleavedSamplingTypeData[] = {
        { 0, "Off" },
        { 1, "3x3" },
        { 2, "5x5" },
    };
    TwType interleavedSamplingType = TwDefineEnum("Interleaved Sampling", interleavedSamplingTypeData, 3);

    TwAddVarRW(tweakBar, "volumeType", volumeType, &vol->volumeType, " label='Volume type'");

    TwAddVarRW(tweakBar, "volumeNear", TW_TYPE_FLOAT, &vol->fogNear, " label='Near Limit' min=0 step=0.1 max=1000 group='Fog'");
    TwAddVarRW(tweakBar, "volumeFar", TW_TYPE_FLOAT, &vol->fogFar, " label='Far Limit' min=0 step=0.1 max=1000 group='Fog'");
    TwAddVarRW(tweakBar, "volumeColor", TW_TYPE_COLOR3F, glm::value_ptr(vol->fogColor), " label='Fog Color' group='Fog'");
    TwDefine(" Main/Fog label='Distance Fog'");
    
    TwAddVarRW(tweakBar, "absoptionTau", TW_TYPE_FLOAT, &vol->absorptionTau, " label='Absorption factor' min=0.0 max=1.0 step=0.001 group='Absorption'");
    TwAddVarRW(tweakBar, "absorptionColor", TW_TYPE_COLOR3F, &vol->absorptionColor, " label='Absorption color' group='Absorption'");
    TwDefine(" Main/Absorption label='Absorption'");
    
    TwAddVarRW(tweakBar, "scatteringSamples", TW_TYPE_INT32, &vol->scatteringSamples, " label='Samples' min=0 max=500 group='Scattering'");
    TwAddVarRW(tweakBar, "scatteringTau", TW_TYPE_FLOAT, &vol->scatteringTau, " label='Scattering factor' min=0.0 max=1.0 step=0.001 group='Scattering'");
    TwAddVarRW(tweakBar, "phaseFunctionType", phaseFunctionType, &vol->phaseFunctionType, " label='Phase function' group='Scattering'");
    TwAddVarRW(tweakBar, "anisotropy", TW_TYPE_FLOAT, &vol->anisotropy, " label='Anisotropy' min=-1.0 max=1.0 step=0.01 group='Scattering'");
    TwAddVarRW(tweakBar, "scatteringZFar", TW_TYPE_FLOAT, &vol->scatteringZFar, " label='Max distance' min=1.0 step=0.1 group='Scattering'");
    TwAddVarRW(tweakBar, "scatteringColor", TW_TYPE_COLOR3F, &vol->scatteringColor, " label='Scattering color' group='Scattering'");
    TwAddVarRW(tweakBar, "interleavedSampling", interleavedSamplingType, &vol->interleavedSampling, " label='Interleaved Sampling' group='Scattering'");
    TwDefine(" Main/Scattering label='Scattering'");
    
    TwAddVarRW(tweakBar, "onlyVolume", TW_TYPE_BOOLCPP, &engine->getPostProcessingData().onlyVolume, " label='Only Volume' group='PostPro'");
    TwAddVarRW(tweakBar, "gamma", TW_TYPE_FLOAT, &engine->getPostProcessingData().gamma, " label='Gamma' min=0.0 step=0.1 group='PostPro'");
    TwAddVarRW(tweakBar, "exposure", TW_TYPE_FLOAT, &engine->getPostProcessingData().exposure, " label='Exposure' min=0.0 step=0.1 group='PostPro'");
    TwDefine(" Main/PostPro label='Post Processing'");
    
    TwAddVarRO(tweakBar, "msShadowMap", TW_TYPE_DOUBLE, &engine->msTime[0], " step=0.1 label='Shadow map (ms)' group='Timer'");
    TwAddVarRO(tweakBar, "msDrawingScene", TW_TYPE_DOUBLE, &engine->msTime[1], " step=0.1 label='Drawing scene (ms)' group='Timer'");
    TwAddVarRO(tweakBar, "msVolumetric", TW_TYPE_DOUBLE, &engine->msTime[2], " step=0.1 label='Volumetric (ms)' group='Timer'");
    TwAddVarRO(tweakBar, "msPostProcess", TW_TYPE_DOUBLE, &engine->msTime[3], " step=0.1 label='Post process (ms)' group='Timer'");
    TwAddVarRO(tweakBar, "msTotal", TW_TYPE_DOUBLE, &engine->msTime[4], " step=0.1 label='Total (ms)' group='Timer'");
    TwDefine(" Main/Timer label='Render Time'");
}


Application::~Application() {
    // engine is singleton and taken care of by itself.
    delete scene;
}


void Application::runMainLoop() {
    double time = glfwGetTime();
    while(!glfwWindowShouldClose(window)) {
        double deltaT = glfwGetTime() - time;
        time += deltaT;
        updateCameraFromKeyboard(deltaT);
        
        scene->animate(deltaT);
        
        engine->render(*scene, camera);
        // Draw Tweak bar
        TwDraw();
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}


void Application::terminate() {
    TwDeleteBar(tweakBar);
    glfwDestroyWindow(window);
    glfwTerminate();
}


void Application::setCloseFlag() {
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void Application::resizeWindow(int width, int height) {
    camera.updateAspectRatio(((float)width)/height);
    engine->resize(width, height);
}


void Application::updateCameraFromKeyboard(float deltaT) {
    float movementSpeedFactor = 1.0;
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS or glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS)
        movementSpeedFactor = 3.0;

    float cameraMoveFactor = CAMERA_MOVE_FACTOR*movementSpeedFactor;
    float cameraRotateFactor = CAMERA_ROTATE_FACTOR*movementSpeedFactor;
    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        glm::vec3 dir = camera.getDirection();
        dir.z = 0.0f;
        dir = glm::normalize(dir);
        camera.translate(dir*cameraMoveFactor*deltaT);
    }
    else if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        glm::vec3 dir = -camera.getDirection();
        dir.z = 0.0f;
        dir = glm::normalize(dir);
        camera.translate(dir*cameraMoveFactor*deltaT);
    }
    
    if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        glm::vec3 right = glm::cross(camera.getDirection(), Z_AXIS);
        camera.translate(glm::normalize(-right)*cameraMoveFactor*deltaT);
    }
    else if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        glm::vec3 right = glm::cross(camera.getDirection(), Z_AXIS);
        camera.translate(glm::normalize(right)*cameraMoveFactor*deltaT);
    }
    
    if(glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
        camera.translate(Z_AXIS*cameraMoveFactor*deltaT);
    }
    else if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
		camera.translate(-Z_AXIS*cameraMoveFactor*deltaT);
    }
    
    if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		camera.rotate(cameraRotateFactor*deltaT, 0.0);
    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		camera.rotate(0.0, cameraRotateFactor*deltaT);
    }
    else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		camera.rotate(-cameraRotateFactor*deltaT, 0.0);
    }
    else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		camera.rotate(0.0, -cameraRotateFactor*deltaT);
    }
}


void Application::onGLFWMouseButtonEvent(GLFWwindow*, int button, int action, int /*mods*/) {
    TwEventMouseButtonGLFW(button, action);
}

void Application::onGLFWMousePositionEvent(GLFWwindow*, double x, double y) {
    TwEventMousePosGLFW(int(x), int(y));
}

void Application::onGLFWKeyEvent(GLFWwindow*, int key, int /*scancode*/, int action, int /*mods*/) {
    if(action != GLFW_PRESS)
        return;
    int twKey = key;
    if(key == GLFW_KEY_ESCAPE)
        twKey = TW_KEY_ESCAPE;
    else if(key == GLFW_KEY_ENTER)
        twKey = TW_KEY_RETURN;
    else if(key == GLFW_KEY_BACKSPACE)
        twKey = TW_KEY_BACKSPACE;
    else if(key == GLFW_KEY_UP)
        twKey = TW_KEY_UP;
    else if(key == GLFW_KEY_DOWN)
        twKey = TW_KEY_DOWN;
    else if(key == GLFW_KEY_LEFT)
        twKey = TW_KEY_LEFT;
    else if(key == GLFW_KEY_RIGHT)
        twKey = TW_KEY_RIGHT;
    
    if(TwKeyPressed(twKey, 0))
        return;
    
    if(key == GLFW_KEY_ESCAPE) {
        Application::getInstance().setCloseFlag();
    }
}

void Application::onGLFWWindowResizeEvent(GLFWwindow*, int width, int height) {
    TwWindowSize(width, height);
    Application::getInstance().resizeWindow(width, height);
}
