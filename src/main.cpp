#include <cstring>

#include "Application.hpp"


// Make sure we can be lazy and use 'int' instead of 'GLint' and so on...
static_assert(std::is_same<int, GLint>::value, "C++ int is not same as GLint");
static_assert(std::is_same<uint, GLuint>::value, "C++ uint is not same as GLuint");
static_assert(std::is_same<uint, GLenum>::value, "C++ uint is not same as GLenum");
static_assert(std::is_same<float, GLfloat>::value, "C++ float is not same as GLfloat");


int main(int argv, char* argc[]) {
    SceneType sceneType = SCENE_SPONZA;
    if(argv == 2) {
        if(strcmp(argc[1], "cubes") == 0)
            sceneType = SCENE_FLOATING_CUBES;
        else if(strcmp(argc[1], "sibenik") == 0)
            sceneType = SCENE_SIBENIK;
        else if(strcmp(argc[1], "spot") == 0)
            sceneType = SCENE_SPOT;
    }

    Application::getInstance().init(sceneType);
    Application::getInstance().runMainLoop();
    Application::getInstance().terminate();
    
    return 0;
}
