# 100% More Volume
## Real-time volumetric lighting in homogeneous participating media using ray marching

This is the result of a project in the course TSBK03 "Advanced Game Programming" at 
Linköping University. It is a real time demo capable of rendering
crepuscular rays, also known as light shafts. 


The implementation uses a ray marching algorithm adapted from Tóth and Umenhoffer's paper "Real-time Volumetric Lighting in Participating Media." (2009).
Implementation is done in C++11 and OpenGL 4.0, using GLFW3 for window managing and AntTweakBar for GUI. CMake is used to generate make files.


The implementation has been tested on Ubuntu 17.10 using GCC 7.2.0 and Windows 7 using MSVC 18.0.31101.0 (Visual Studio 12 2013).

A video of the demo can be seen here: [https://youtu.be/Bafoekti34Y](https://youtu.be/Bafoekti34Y)

## How to compile

### Linux/GCC
You will need the usual libraries needed for OpenGL programming (OpenGL libraries, X11 libraries, etc.). 
In addition to that you will need, GLFW3, CMake, make, and GCC/G++.

To download the libraries on Ubuntu, you will need to run something like (assumes GCC and make are already installed with the distro):
```
sudo apt-get install cmake libx11-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev libxrandr-dev libxext-dev libxcursor-dev libxinerama-dev libxi-dev libglfw3
```


To compile, run the following in the project root:
```
mkdir build
cd build
cmake ..
make
make shaders
make resources
```


The executable called `volumeapp` will be located in the build directory. The two additional targets `shaders` and `resources` will copy the res/ and shaders/ directories to the build directory.
Run the application using:

```
./volumeapp SCENE
```

where SCENE is the name of the scene you want to load. If no scene is
specified, a default scene will be loaded. See below for the names of the scenes.

### Windows/MSVC
For Windows the implementation has only ben compiled with Visual Studio 12 2013. More modern VS versions ought to work, but you might need to fix some things. 
The AntTweakBar.dll file is pre-compiled with MSVC 2013 (I am not sure how much of a difference it makes), so if you compile with a more recent VS you might 
need to download another pre-compiled AntTweakBar from their website or build it yourself. 
[AntTweakBar download page](http://anttweakbar.sourceforge.net/doc/tools:anttweakbar:download). 
Replace the AntTweakBar.dll in the lib/ directory. 
It will be automatically copied to the output directory after the compilation.

To compile on Window you need VS 2013, CMake and a decent terminal(Cygwin/MinGW).
In the terminal run in the project root:
```
mkdir build
cd build
cmake ..
```

This will generate a VS solution with the targets as projects in the solution.
Open the solution file (`VolumeRender.sln`) in VS 2013.

Build the project `volumeapp` to make the executable and copy the AntTweakBar link library. Then build projects `shaders` and `resources` to copy the shaders/
and res/ directories to the output directory. The result will be located in the build/Debug directory (or build/Release if a release build type was chosen).

Then run the executable from the build/Debug directory using:
```
./volumeapp SCENE
```
where SCENE is the name of the scene you want to load. If no scene is
specified, a default scene will be loaded. See below for the names of the scenes.

# Scenes
The demo includes four scenes:

***cubes*** - A simple scene with a spot light shining on 64 rotating cubes. The scene is inspired by a NVIDIA Game Works test scene.

***spot*** - A scene with three spotlight pointed at a sphere. Shows colored and multiple lights.

***sibenik*** - A cathedral made by Marko Dabrovic. Uses two spot lights.

***sponza*** - The classical Sponza test scene by Frank Meinl. Uses one point light in the middle of the atrium.


To run the demo with a specific scene, just pass the name of the scene as an argument to the program like `./volumeapp sponza`.

# Controls
The camera can be controlled using the ASDW-keys to move in the XY-plane and E/C-keys to move up/down. The keyboard arrows are used to rotate the camera. Press shift to move faster. Press ESC to exit the application.

# Tweak Bar
The tweak bar contains the following settings:
* __Volume type__ Specifies the type of volumetric rendering. Choose between:
  - __Off__
  - __Distance fog__
  - __Absorption__
  - __Scattering__

#### Distance fog
- __Near limit__ Defines the near limit in meters from the camera where the scene will start to transition into the fog color.
- __Far limit__ Defines the far limit in meters from the camera where then scene will be completely replaced by the fog color.
- __Fog color__ The color the distance fog will have

#### Absorption
These settings only work when the Volume type is set to Absorption or Scattering.
- __Absorption factor__ The probability of a absorption event happening per meter. Higher value means more light will be absorbed by the volume.
- __Absorption color__ The color the transmitted rays will have. In other words, the complementary color of the color that will be absorbed.

#### Scattering 
These settings only work when the Volume type is set to Scattering.
- __Samples__ The number of samples per pixel when doing the ray marching to calculate in-scattering light. Higher number means slower performance, but less noise.
- __Scattering factor__ The probability of a scattering event happening per meter. A higher value means more scattering, but darker scene. 
- __Phase function__ The phase function to use. Choose between:
  * __Henyey-Greenstein__ (Analytical, tweakable anisotropy value)
  * __Mie-Hazy__ (Anisotropic, light fog)
  * __Mie-Murky__ (Anisotropic, dense fog)
  * __Rayleigh__ (Isotropic, atmospheric scattering)
- __Anisotropy__ Parameter g in the Henyey-Greenstein phase function. Only works when that phase function is in use. Positive value means anisotropic forward-scattering, 0 means isotropic scattering, negative value mean anisotropic back-scattering.
- __Max distance__ The max ray marching distance. Useful when the viewing frustum's far limit is far away. Lower value means better quality when background is far away from camera, but might lead to clipping the light shafts.
- __Scattering color__ The color of the in-scattered light.
- __Interleaved sampling__ Setting to control interleaved sampling. Choose between:
  * __Off__
  * __3x3__ - Use the 8 closest pixels
  * __5x5__ - Use the 24 closest pixels
    Using interleaved sampling leads to more course grained noise, making it possible to lower the sample count to save performance.

#### Post processing
- __Only volume__ Checkbox to choose to only render volumetric pass. Only works when Volume Type is set to distance fog or Scattering.
- __Gamma__ The gamma applied in the tone mapping.
- __Exposure__ The exposure setting used in the tone mapping.


#### Render Time
- __Shadow Map (ms)__ Render time for the shadow maps. Usually 0 if there is no need to rerender them.
- __Drawing Scene (ms)__ Render time for the actual scene.
- __Volumetric (ms)__ Render time for the volumetric in-scattering pass.
- __Post Process (ms)__ Render time for the post processing pass, including tone mapping, compositing and interleaved sampling.
- __Total (ms)__ The total render time of one frame.