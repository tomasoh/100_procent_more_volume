#version 400 core

layout(location = 0) in vec3 inPosition;

uniform mat4 modelToWorld;
uniform mat4 worldToProj;

void main() {
    gl_Position = worldToProj * modelToWorld * vec4(inPosition, 1.0);
}
