#version 400 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 18) out; // 3 verts * 6 directions

uniform mat4 cubeMapMatrices[6];

out vec4 fragmentPosition;

void main() {
    for(int direction = 0; direction < 6; ++direction) {
        gl_Layer = direction;
        for(int vert = 0; vert < 3; ++vert) {
            fragmentPosition = gl_in[vert].gl_Position;
            gl_Position = cubeMapMatrices[direction] * fragmentPosition;
            EmitVertex();
        }
        EndPrimitive();
    }
}
