#version 400 core

in vec4 fragmentPosition;

uniform vec3 lightPosition;
uniform float zFar;

void main() {
    float dist = distance(lightPosition, fragmentPosition.xyz);
    // Make a mapping to the 0-1 range
    gl_FragDepth = dist/zFar;
}
