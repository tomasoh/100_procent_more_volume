#version 400 core

// Keep in sync with constant in Lights.hpp
const int MAX_LIGHTS = 8;
const float POINT_SHADOW_BIAS = 0.05;
const float SPOT_SHADOW_BIAS = 0.00004;

const int NONE = 0;
const int DISTANCE_FOG = 1;
const int ABSORPTION = 2;
const int SCATTERING = 3;

const int POINT_LIGHT = 0;
const int SPOT_LIGHT = 1;

struct Material {
    float diffuseFactor;
    vec3 specularColor;
    float specularFactor;
    float specularSharpness;
    float ambientFactor;
};

struct Light {
    int type;
    vec3 position;
    vec3 direction; // Only for spot lights
    float spotCutOff; // Only for spot lights, cut off expressed as cosine of angle
    vec3 color;
    float intensity;
    float shadowMapZFar;
    mat4 spotWorldToProj; // Only for spot
    sampler2D spotShadowMap;
    samplerCube pointShadowMap;
};

in vec3 fragPosition;
in vec3 fragNormal;
in vec2 fragUV;


uniform Material material;
uniform sampler2D diffuseTex;
uniform int numLights;
uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;

uniform int volumeType;

// Absorption specific parameters
uniform float absorptionTau;
uniform vec3 absorptionColor;
uniform float scatteringTau;

out vec4 fragColor;


const vec3 pointSampleOffsetDirections[20] = vec3[] (
   vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1), 
   vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
   vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
   vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
   vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
); 

vec3 absorptionTransmittance(float dist) {
    return absorptionColor*exp(-dist*(absorptionTau+scatteringTau));
}

float shadowFactor(Light light, vec3 point) {
    if(light.type == POINT_LIGHT) {
        vec3 fragLightDir = normalize(light.position - point);
        float lightDist = distance(light.position, point);
        float shadow = 0.0;
        float viewDistance = distance(viewPos, fragPosition);
        float diskRadius = (1.0 + (viewDistance / light.shadowMapZFar)) / 250.0;
        for(int i = 0; i < pointSampleOffsetDirections.length(); ++i) {
            float shadowDist = texture(light.pointShadowMap, -fragLightDir + pointSampleOffsetDirections[i]*diskRadius).r;
            shadowDist *= light.shadowMapZFar;
            if(lightDist > shadowDist + POINT_SHADOW_BIAS)
                shadow += 1.0/float(pointSampleOffsetDirections.length());
        }
        return shadow;
    }
    else if (light.type == SPOT_LIGHT) {
        vec3 fragLightDir = normalize(light.position - point);
        float cosAngle = dot(fragLightDir, normalize(-light.direction));
        if(cosAngle > light.spotCutOff) {
            vec4 pointLightSpace = light.spotWorldToProj * vec4(point, 1.0);
            vec3 lightProjCoords = pointLightSpace.xyz / pointLightSpace.w;
            lightProjCoords = lightProjCoords * 0.5 + 0.5;
            float shadowDist = texture(light.spotShadowMap, lightProjCoords.xy).r;
            float lightDist = lightProjCoords.z;
            return lightDist > shadowDist + SPOT_SHADOW_BIAS ? 1.0 : 0.0;
        }
        else {
            // Outside of spot circle, is in shadow
            return 1.0;
        }
    }
    // Something is wrong!
    return 1.0; 
}


/**
 * Calculate the lighting one light contributes with
 */
vec3 calculateLighting(Light light, vec3 normal, vec3 viewDir) {
    vec3 fragLightDir = normalize(light.position - fragPosition);
    vec3 halfwayDir = normalize(fragLightDir + viewDir);
    
    float diffuse = max(dot(normal, fragLightDir), 0.0) * material.diffuseFactor;
    // Blinn-Phong
    float specular = pow(max(dot(normal, halfwayDir), 0.0), material.specularSharpness) * material.specularFactor;
    
    float dist = distance(light.position, fragPosition);
    float attenuation = 1.0/(dist*dist);
    
    float shadow = shadowFactor(light, fragPosition);
    
    vec3 lightContrib = (diffuse + specular) * light.color * light.intensity;
    return (1.0 - shadow) * attenuation * lightContrib;
}


void main() {
    vec3 normal = normalize(fragNormal);
    vec3 viewDir = normalize(viewPos - fragPosition);
    vec3 diffuseColor = texture(diffuseTex, fragUV).rgb;
    
    vec3 lightRes = vec3(material.ambientFactor);
    for(int i = 0; i < numLights; ++i) {
        vec3 lightContrib = calculateLighting(lights[i], normal, viewDir);
        if(volumeType == ABSORPTION || volumeType == SCATTERING) {
            lightContrib *= absorptionTransmittance(distance(viewPos, fragPosition));
        }
        lightRes += lightContrib;
    }

    lightRes *= diffuseColor;
    if(volumeType == ABSORPTION || volumeType == SCATTERING) {
        lightRes *= absorptionTransmittance(distance(viewPos, fragPosition));
    }
    fragColor = vec4(lightRes, 1.0);
}
