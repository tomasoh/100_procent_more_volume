#version 400 core

in vec2 fragmentTexCoord;
out vec4 out_Color;

const float M_PI = 3.141592653589793238;

const int NONE = 0;
const int DISTANCE_FOG = 1;
const int ABSORPTION = 2;
const int SCATTERING = 3;


const int HENYEY_GREENSTEIN = 0;
const int MIE_HAZY = 1;
const int MIE_MURKY = 2;
const int RAYLEIGH = 3;

// Keep in sync with constant in Lights.hpp
const int MAX_LIGHTS = 8;
const float POINT_SHADOW_BIAS = 0.02;
const float SPOT_SHADOW_BIAS = 0.00004;

const int POINT_LIGHT = 0;
const int SPOT_LIGHT = 1;


uniform int volumeType;
uniform int phaseFunctionType;

uniform sampler2D zBuffer;

uniform mat4 projectionToWorld;
uniform vec3 cameraPosition;
uniform float screenWidth;
uniform float screenHeight;

struct Light {
    int type;
    vec3 position;
    vec3 direction; // Only for spot lights
    float spotCutOff; // Only for spot lights, cut off expressed as cosine of angle
    vec3 color;
    float intensity;
    float shadowMapZFar;
    mat4 spotWorldToProj; // Only for spot
    sampler2D spotShadowMap;
    samplerCube pointShadowMap;
};

uniform int numLights;
uniform Light lights[MAX_LIGHTS];


// Distance fog specific parameters
uniform float fogNear;
uniform float fogFar;
uniform vec3 fogColor;

// Absorption specific parameters
uniform float absorptionTau;
uniform vec3 absorptionColor;

// Scattering specific parameters
uniform int scatteringSamples;
uniform float scatteringTau;
uniform float anisotropy;
uniform float scatteringZFar;
uniform vec3 scatteringColor;

/**
 * Calulate the fragment position in world coordinates.
 */
vec3 fragmentWorldPos(float depthValue) {
    vec4 ndcCoords = vec4(
        2.0*(gl_FragCoord.x / screenWidth) - 1.0,
        2.0*(gl_FragCoord.y / screenHeight) - 1.0,
        2.0*depthValue - 1.0,
        1.0);

    vec4 worldCoords = projectionToWorld * ndcCoords;
    return worldCoords.xyz / worldCoords.w;
}


/**
 * Returns a value between 0.0 and 1.0, from no fog to full fog
 */
float simpleDistanceFog(float dist) {
    float fog = (dist - fogNear) / (fogFar - fogNear);
    fog = clamp(fog, 0.0, 1.0);
    return fog;
}


vec3 absorptionTransmittance(float dist) {
    return absorptionColor*exp(-dist*(absorptionTau+scatteringTau));
}


float shadowFactor(Light light, vec3 point) {
    if(light.type == POINT_LIGHT) {
        vec3 fragToLight = light.position - point;
        float lightDist = length(fragToLight);
        float shadowDist = texture(light.pointShadowMap, -fragToLight).r;
        shadowDist *= light.shadowMapZFar;
        return lightDist > shadowDist + POINT_SHADOW_BIAS ? 1.0 : 0.0;
    }
    else if (light.type == SPOT_LIGHT) {
        vec3 fragLightDir = normalize(light.position - point);
        float cosAngle = dot(fragLightDir, normalize(-light.direction));
        if(cosAngle > light.spotCutOff) {
            vec4 pointLightSpace = light.spotWorldToProj * vec4(point, 1.0);
            vec3 lightProjCoords = pointLightSpace.xyz / pointLightSpace.w;
            lightProjCoords = lightProjCoords * 0.5 + 0.5;
            float shadowDist = texture(light.spotShadowMap, lightProjCoords.xy).r;
            float lightDist = lightProjCoords.z;
            return lightDist > shadowDist + SPOT_SHADOW_BIAS ? 1.0 : 0.0;
        }
        else {
            // Outside of spot circle, is in shadow
            return 1.0;
        }
    }
    // Something is wrong!
    return 1.0; 
}


/**
 * Henyey- Greenstein phase function
 */
float phaseFunctionHenyeyGreenstein(vec3 inDir, vec3 outDir) {
    float cosAngle = dot(inDir, outDir) / (length(inDir)*length(outDir));
    float nom = 1 - anisotropy*anisotropy;
    float denom = 4 * M_PI * pow(1 + anisotropy*anisotropy - 2*anisotropy*cosAngle, 1.5);
    return nom/denom;
}


/**
 * Rayleigh scattering phase function
 */
float phaseFunctionRayleigh(vec3 inDir, vec3 outDir) {
    float cosAngle = dot(inDir, outDir) / (length(inDir) * length(outDir));
    float nom = 3.0*(1.0 + cosAngle*cosAngle);
    float denom = 16.0 * M_PI;
    return nom/denom;
}


/**
 * Mie-Hazy phase function
 */
float phaseFunctionMieHazy(vec3 inDir, vec3 outDir) {
    float cosAngle = dot(inDir, outDir) / (length(inDir) * length(outDir));
    float x = (1.0 + cosAngle)/2.0;
    float x2 = x*x;
    float x4 = x2*x2;
    float x8 = x4*x4;
    float nom = 0.5 + 4.5*x8;
    float factor = 1.0/(4.0 * M_PI);
    return nom*factor;
}


/**
 * Mie-Murky phase function
 */
float phaseFunctionMieMurky(vec3 inDir, vec3 outDir) {
    float cosAngle = dot(inDir, outDir) / (length(inDir) * length(outDir));
    float x = (1.0 + cosAngle)/2.0;
    float x2 = x*x;
    float x4 = x2*x2;
    float x8 = x4*x4;
    float x16 = x8*x8;
    float x32 = x16*x16;
    float nom = 0.5 + 16.5*x32;
    float factor = 1.0/(4.0 * M_PI);
    return nom*factor;
}


float phaseFunction(vec3 inDir, vec3 outDir) {
    if(phaseFunctionType == HENYEY_GREENSTEIN) {
        return phaseFunctionHenyeyGreenstein(inDir, outDir);
    }
    else if(phaseFunctionType == MIE_HAZY){
        return phaseFunctionMieHazy(inDir, outDir);
    }
    else if(phaseFunctionType == MIE_MURKY){
        return phaseFunctionMieMurky(inDir, outDir);
    }
    else if(phaseFunctionType == RAYLEIGH){
        return phaseFunctionRayleigh(inDir, outDir);
    }
    return 0.0;
}


/**
 * 2D Pseudo random number generator from:
 * The Book of Shaders by Patricio Gonzalez Vivo and Jen Lowe
 * https://thebookofshaders.com/10/
 * Returns a pseudo random value between 0.0 and 1.0
 */
float random(vec2 co) {
    return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}


/**
 * Volumetric scattering function based on "Real-time Volumetric Lighting 
 * in Participating Media" by Balázs Tóth and Tamás Umenhoffer.
 */
vec3 volumetricScattering(vec3 fragPosition, Light light) {
    vec3 result = vec3(0.0, 0.0, 0.0);
    vec3 camToFrag = fragPosition - cameraPosition;
    if(length(camToFrag) > scatteringZFar) {
        camToFrag = normalize(camToFrag) * scatteringZFar;
    }
    vec3 deltaStep = camToFrag / (scatteringSamples+1);
    vec3 fragToCamNorm = normalize(cameraPosition - fragPosition);
    vec3 x = cameraPosition;
    
    // Randomize the sampling points along the ray to trade flickering
    // for plain noise
    float rand = random(fragPosition.xy+fragPosition.z);
    x += (deltaStep*rand);
    
    for(int i = 0; i < scatteringSamples; ++i) {
        float visibility = 1.0 - shadowFactor(light, x);
        vec3 lightToX = x - light.position;
        float lightDist = length(lightToX);
        float omega = 4 * M_PI * lightDist * lightDist;
        vec3 Lin = absorptionTransmittance(lightDist) * visibility * light.color * light.intensity / omega;
        vec3 Li = Lin * scatteringTau * scatteringColor * phaseFunction(normalize(lightToX), fragToCamNorm);
        result += Li * absorptionTransmittance(distance(x, cameraPosition)) * length(deltaStep);
        x += deltaStep;
    }
    
    return result;
}



void main(void) {
    vec4 volumetricColor = vec4(0.0, 0.0, 0.0, 1.0);
    float depthValue = texture(zBuffer, fragmentTexCoord).r;
    vec3 fragPosition = fragmentWorldPos(depthValue);
    
    float fragCamDist = distance(fragPosition, cameraPosition);
    
    if(volumeType == DISTANCE_FOG) {
        float distanceFogFactor = simpleDistanceFog(fragCamDist);
        volumetricColor = vec4(fogColor, distanceFogFactor);
    }
    else if(volumeType == ABSORPTION) {
        // Handled in default shader
    }
    else if(volumeType == SCATTERING) {
        for(int i = 0; i < numLights; ++i) {
            volumetricColor += vec4(volumetricScattering(fragPosition, lights[i]), 0.0);
        }
    }
    
    out_Color = volumetricColor;
}
