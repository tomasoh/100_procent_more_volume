#version 400 core

layout(location = 0) in vec3 inPosition;

uniform mat4 modelToWorld;

void main() {
    gl_Position = modelToWorld * vec4(inPosition, 1.0);
}
