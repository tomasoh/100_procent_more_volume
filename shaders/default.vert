#version 400 core

in vec3 inPosition;
in vec3 inNormal;
in vec2 inTexCoord;

uniform mat4 modelToWorld;
uniform mat4 modelToProj;

out vec3 fragPosition;
out vec3 fragNormal;
out vec2 fragUV;

void main() {
    fragPosition = vec3(modelToWorld * vec4(inPosition, 1.0));
    fragNormal = mat3(modelToWorld)*inNormal;
//     fragNormal = mat3(transpose(inverse(modelToWorld)))*inNormal; // TODO: move to CPU
    fragUV = inTexCoord;
    
    gl_Position = modelToProj*vec4(inPosition, 1.0);
}
