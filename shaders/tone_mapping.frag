#version 400 core

const int NONE = 0;
const int DISTANCE_FOG = 1;
const int ABSORPTION = 2;
const int SCATTERING = 3;

in vec2 fragmentTexCoord;
out vec4 out_Color;

uniform sampler2D hdrTexture;
uniform sampler2D volumetricTexture;

uniform int volumeType;

uniform int onlyVolume; // Render only the volume buffer
uniform float gamma;
uniform float exposure;

uniform int interleavedSampling;

void main(void) {
    vec3 hdrColor = vec3(0.0, 0.0, 0.0);
    if(onlyVolume == 0) {
        hdrColor = texture(hdrTexture, fragmentTexCoord).rgb;
    }
    vec4 volumetricColor = texture(volumetricTexture, fragmentTexCoord);
    
    if(volumeType == DISTANCE_FOG) {
        hdrColor = mix(hdrColor, volumetricColor.rgb, volumetricColor.a);
    }
    else if(volumeType == SCATTERING) {
        if(interleavedSampling == 1) {
            // 3x3 interleaved sampling. Use in-between pixel lookups for efficiency
            vec2 pixelSize = 1.0 / textureSize(volumetricTexture, 0);
            vec3 scattering = 0.5*volumetricColor.rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(-0.5*pixelSize.x, -pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(pixelSize.x, -0.5*pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(0.5*pixelSize.x, pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(-pixelSize.x, 0.5*pixelSize.y)).rgb;
            hdrColor += (2.0 / 9.0) * scattering;
        }
        else if(interleavedSampling == 2) {
            // 5x5 interleaved sampling. Use in-between pixel lookups for efficiency
            // Lookup pattern is devided into 9 texture lookups like:
            // ## # ##
            // ## # ##
            //
            // ## # ##
            //
            // ## # ##
            // ## # ##
            vec2 pixelSize = 1.0 / textureSize(volumetricTexture, 0);
            vec3 scattering = 0.5*volumetricColor.rgb;
            // Interpolate four pixels
            scattering += 2.0*texture(volumetricTexture, fragmentTexCoord + vec2(-1.5*pixelSize.x, -1.5*pixelSize.y)).rgb;
            scattering += 2.0*texture(volumetricTexture, fragmentTexCoord + vec2(1.5*pixelSize.x, -1.5*pixelSize.y)).rgb;
            scattering += 2.0*texture(volumetricTexture, fragmentTexCoord + vec2(-1.5*pixelSize.x, 1.5*pixelSize.y)).rgb;
            scattering += 2.0*texture(volumetricTexture, fragmentTexCoord + vec2(1.5*pixelSize.x, 1.5*pixelSize.y)).rgb;
            
            // Interpolate two pixels
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(pixelSize.x, 1.5*pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(pixelSize.x, -1.5*pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(1.5*pixelSize.x, pixelSize.y)).rgb;
            scattering += texture(volumetricTexture, fragmentTexCoord + vec2(-1.5*pixelSize.x, pixelSize.y)).rgb;
            hdrColor += (2.0/25.0) * scattering;
        }
        else {
            // Interleaved sampling off
            hdrColor += volumetricColor.rgb;
        }
    }
    
    // Exposure mapping
    hdrColor = vec3(1.0) - exp(-hdrColor*exposure);

    // Gamma correction
    hdrColor = pow(hdrColor, vec3(1.0/gamma));

    out_Color = vec4(hdrColor, 1.0);
}
